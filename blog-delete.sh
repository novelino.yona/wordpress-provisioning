#!/bin/bash

SERVICE_IP=$(kubectl get svc nginx-ingress-controller -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
DOMAINNAME=$(kubectl get ing -nwordpress worpdress-ingress -o jsonpath='{.spec.rules[0].host}')
echo "deleting host entries for ${DOMAINNAME}"
sudo sed -i -e "s/${SERVICE_IP} ${DOMAINNAME}\n//g" /etc/hosts

echo "delete ingress helm"
helm delete nginx-ingress

echo "delete wordpress deployment yaml"
kubectl delete -f wordpress.yaml

echo "delete mysql statefulsets yaml"
kubectl delete -f mysql.yaml

echo "deleting container cluster"
ansible-playbook wpprovision.yml --tags wordpress-provision --extra-vars state=absent



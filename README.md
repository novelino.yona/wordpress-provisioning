# Wordpress Provisioning using GKE

This repo enables you to create your own wordpress site automatically. Which in this repo you must have Google Cloud Platform account since we're using Google Kubernetes Engine (GKE) that part of Google Cloud Platform.

## Prerequisite
To enable This you must has all of the package below :
```
python
ansible with version greater than 2.5 (version that has GCP library)
gcloud
kubectl 
```
please install those package before continuing reading this readme or using this repo

## How To Use

##### 1. Pre-Installation
For the very first time using this script for auto provision, we need to install pip package that listed below
```
request
google-auth
```
those two pip package enable ansible to contact Google Cloud Platform to made a modification. To install those package, it's easy as you just need to execute command that stated below
```
pip -r requirements.txt
```

you need to crate serviceaccount keys on the GCP since ansible will use that key and email address (as a user) that correspondence on the key to authenticate ansible with GCP, please refer to [this documentation](https://cloud.google.com/iam/docs/creating-managing-service-account-keys) to create it 

please set the file `vars.yml` with the setting for your own GKE information with description stated below
```
cluster_name: test-gke-autoprovision          # define your cluster name
project_name: utility-time-217709             # define the project name where your cluster should be hosted
node_count: 2                                 # Worker node Count
state: present                                # state for the cluster (absent/present)
zone: asia-southeast1-a                       # define the zone where your cluster should be hosted
machine_type: n1-standard-1                   # your machine type for each node
disk_size: 20                                 # size of the disk for each node
```
if you are already install `request` and `google-auth` pip package, create the key, and set the `vars.yml` you are ready to execute the real thing.

##### 2. Installation
* For Provisioning the wordpress, please execute this command and will take a time until the wordpress is provisioned
    ```
    $ export GCP_SERVICE_ACCOUNT_FILE=<serviceaccountkeyfile>
    $ export GKEUSER=<Serviceaccountemail>
    $ ./blog-up <mysqlpassword> <wpdomainname>
    ```
    *Note: at the end of the script you may require to input your root password since it'll modify the hostfile in order to use the domainname instead of IP address for accessing the Wordpress since this script also enable ingress in kubernetes to point the wordpress page

    Final output from the script above should be like below
    ```
    Please Setup your Wordpress on http://<wpdomainname> or http://1.2.3.4/
    ```
    So open your browser and go to the website address

* For Resizing the wordpress pods, it's as simple as executing
    ```
    $ ./blog-resize.sh <wordpresspodscount>
    ```
    it will display the output that you are already scale your wordpress pods and show the current pods running on your GKE (it'll take time until all of the pods are on `Running` State)
    ```
	$ ./blog-resize.sh 5
	your wordpress pods now has 5 replicas
	NAME                         READY   STATUS              RESTARTS   AGE
	mysql-0                      1/1     Running             0          3m26s
	wordpress-78f8b585f4-698c6   0/1     Running             0          7s
	wordpress-78f8b585f4-cmfmx   0/1     Running             0          7s
	wordpress-78f8b585f4-g5kff   0/1     ContainerCreating   0          7s
	wordpress-78f8b585f4-r89ks   0/1     ContainerCreating   0          7s
	wordpress-78f8b585f4-wdjhp   1/1     Running             0          3m24s
    ```
* For Purging the Cluster, execute the command below
  ```
  $ ./blog-delete.sh
  ```
  it'll take a time until the cluster is destroyed

#!/bin/bash

if ! [[ "$1" =~ ^[0-9]+$ ]]; then
	echo "you must specify replica set quantity as integer"
	echo "usage ./blog-resize.sh <replicasnumber (in integer)>"
	exit 1
fi

kubectl scale deployments wordpress --replicas=$1 -nwordpress >> /dev/null

echo "your wordpress pods now has $1 replicas"
kubectl get pods -nwordpress
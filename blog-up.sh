#!/bin/bash

if [[ -z $1 || -z $2 ]]; then
	echo "you must specify the mysql password and domain name for wordpress"
	echo "usage ./blog-up.sh <yourmysqlpassword> <domainname>"
	exit 1
fi

password=`echo -n $1  | base64`
domainname=$2

#edit secret yaml
rm -f mysql-secret.yaml temp.yaml  
( echo "cat <<EOF >mysql-secret.yaml";
  cat template-secret.yaml;
  echo "EOF";
) >temp.yaml
. temp.yaml


#edit ingress yaml
rm -f wordpress-ingress.yaml temp.yaml  
( echo "cat <<EOF >wordpress-ingress.yaml";
  cat template-ingress.yaml;
  echo "EOF";
) >temp.yaml
. temp.yaml

ansible-playbook wpprovision.yml


echo "waiting you wordpress is ready"
WP_IP=$(kubectl get svc wordpress -n wordpress -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
SERVICE_IP=$(kubectl get svc nginx-ingress-controller -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
while [[ -z $SERVICE_IP || -z $WP_IP ]]; do
	sleep 5
	SERVICE_IP=$(kubectl get svc nginx-ingress-controller -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
	WP_IP=$(kubectl get svc wordpress -n wordpress -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
done 
sleep 30

echo "Setting Up host file to point website"
echo "$SERVICE_IP ${domainname}" | sudo tee -a /etc/hosts

clear
echo "Please Setup your Wordpress on http://${domainname} or http://${WP_IP}"


